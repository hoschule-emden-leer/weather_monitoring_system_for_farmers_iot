from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
from pyspark.sql.types import FloatType

def Gdd(df, spark):
    df_spark1 = spark.createDataFrame(df)
    df_spark1.createOrReplaceTempView('Operations1')
    MinMax = spark.sql("""
                SELECT DATE(Date_Time) as Date,
                       ROUND(MAX(Temperature), 2) AS max_temp,
                       ROUND(MIN(Temperature), 2) AS min_temp
                FROM Operations1
                GROUP BY Date
            """)
    # Register the GDD function as a UDF
    udf_calculate_gdd = udf(calculate_gdd, FloatType())
    # Add a new column for GDD
    MinMax = MinMax.withColumn('GDD', udf_calculate_gdd('max_temp', 'min_temp'))
    MinMax.show()

# Function to calculate GDD
def calculate_gdd(Tmax, Tmin, Tbase=10):
    return ((Tmax + Tmin) / 2) - Tbase if Tmax > Tbase else 0
