import time
import random
import pandas as pd
import pytz
import datetime
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql.functions import udf
from pyspark.sql.types import FloatType
from notification import notification_data
from GDD import Gdd

spark = SparkSession.builder.appName("readTextFile").getOrCreate()

# Define the attributes
attributes = ['Temperature', 'Humidity', 'Wind_Speed', 'Rainfall', 'Light_Intensity', 'Yield', 'Soil_Moisture', 'Stations','visibility']

# Define the ranges for each attribute (you may adjust these as needed)
ranges = {
    'Temperature': (4, 12),  # in degrees Celsius
    'Humidity': (0.5, 4.9),  # in percentage
    'Wind_Speed': (11.1, 28.9),  # in m/s
    'Rainfall': (41, 126),  # in mm
    'Light_Intensity': (300, 600),  # in lux
    'Yield': (50, 100),  # in percentage
    'Soil_Moisture': (4, 8),  # in percentage
    'Stations': [
        "61536,E5906,58325,90685358245",
        "19530,C9379,19521,45932745454",
        "12345,C6789,12345,12345678901",
        "23456,D7890,23456,23456789012",
        "34567,E8901,34567,34567890123",
        "45678,F9012,45678,45678901234",
        "56789,G0123,56789,56789012345",
        "67890,H1234,67890,67890123456",
        "78901,I2345,78901,78901234567",
        "89012,J3456,89012,89012345678"],
    'visibility': (1.1, 49.9)
}

# Function to generate random weather data
def generate_data():
    data = {}
    for attribute in attributes:
        if attribute == 'Stations':
            data[attribute] = random.choice(ranges[attribute])  # Select a random station
        else:
            data[attribute] = random.uniform(*ranges[attribute])
    return data

def generate_weather_data():
    df = pd.DataFrame(columns=['Date_Time'] + attributes)  # Create an empty DataFrame outside the loop
    print(df.columns.tolist())  # Print the column headers only once
    start_time_alert = time.time()
    start_time_gdd = time.time()
    while True:
        data = generate_data()
        now = datetime.datetime.now(pytz.timezone('CET'))
        data['Date_Time'] = now.strftime('%Y-%m-%d %H:%M:%S')
        df_temp = pd.DataFrame([data], columns=['Date_Time'] + attributes)  # Create a temporary DataFrame for each data point
        df = df._append(df_temp, ignore_index=True)  # Append the temporary DataFrame to the main DataFrame
        print(df_temp.to_string(index=False, header=False))  # Print the temporary DataFrame without the index and headers
        if time.time() - start_time_alert > (60*60):  # 10 seconds have passed
            notification_data(df)
            start_time_alert = time.time()  # Reset the alert timer
        if time.time() - start_time_gdd > 5:  # 24 hours have passed
            Gdd(df, spark)
            start_time_gdd = time.time()  # Reset the GDD timer
        time.sleep(2)  # wait for 2 seconds
        df.to_csv("output-data.csv")

generate_weather_data()
