from pyspark.sql.functions import split
from pyspark.sql import SparkSession
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.regression import LinearRegression
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType

# Create a SparkSession
spark = SparkSession.builder.appName("readTextFile").getOrCreate()


# Define a UDF to convert the vectors to strings
vector_to_string = udf(lambda vector: str(vector.toArray()), StringType())

# Load the data
df = spark.read.csv("Input.csv", header=True, inferSchema=True)

# Define the assembler
assembler = VectorAssembler(
    inputCols=['feelslike','dew','humidity','precip','snow','snowdepth','windgust','windspeed','winddir'],
    outputCol='features')

# Transform the data
output = assembler.transform(df)

# Select the features and the target variable
final_data = output.select('features', 'temp')

# Split the data into training and testing sets
train_data, test_data = final_data.randomSplit([0.7, 0.3])

# Create a Linear Regression Model object
lr = LinearRegression(labelCol='temp')

# Fit the model to the data
lrModel = lr.fit(train_data)

# Make predictions
predictions = lrModel.transform(test_data)

#lrModel.save("./Regression_models")
# Add a column for the predicted temperature
predictions = predictions.withColumn('predicted_temp', predictions['prediction'])

# Convert the vectors to strings in the predictions DataFrame
predictions = predictions.withColumn('features', vector_to_string(predictions['features']))

# Convert the DataFrame to a pandas DataFrame
predictions_pd = predictions.select('features', 'temp', 'predicted_temp').toPandas()

# Write the pandas DataFrame to a CSV file
predictions_pd.to_csv('mangesh/output.csv', index=False)
