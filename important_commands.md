# Commands
## Docker related
1. To build docker image from docker file: `docker build --tag <image_name>:tag`
2. To inspect a docker container: `docker inspect <container_id>`
3. To check the running containers: `docker ps`
4. To check all the containers: `docker ps -a` 
5. To start the container: docker start `<container id>` 
6. To stop the container: `docker stop <container id>` 
7. To go inside running container: `docker exec -it <container id> bin/bash` 
8. To copy file from one container to other: `docker cp <file_path_to_be_copied> <file_path_to_be_save>`
9. To running a container in detached mode: `docker run -d <container_name>`
10. To check all docker networks: `docker network ls`
11. To create a new network: `docker network create Mysql<network name>`
12. To run mysql: `docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql`
13. To run mysql image: `docker run --name mysq_db -e MYSQL_ROOT_PASSWORD=root -p 3333:3306 -d mysql
docker exec -it 6f31cee7baae(mysql-id) /bin/bash`


## Container Related
1. To list down files: `ls` 
2. To go inside folder: `cd <folder_name>` 
3. To come out from folder: `cd ..` 
4. To rename the file: `mv <file_old_name> <file_new_name>` 
5. To copy the file: `cp <file_to_be_copied> <Copied_file>` 

## Git commands
1. To add a file for commit: `git add <file_name>`
2. To commit the changes onto local repository: `git commit -m <message>` 
3. To push the changes to remote repository: `git push origin main` 