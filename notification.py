import pandas as pd
from pyspark.sql import functions as F
from pyspark.sql import SparkSession
from mail_sender import send_email

# Create a SparkSession
spark = SparkSession.builder.appName("readTextFile").getOrCreate()

def notification_data(df):
    # Convert the pandas DataFrame to a Spark DataFrame
    df_spark = spark.createDataFrame(df)
    df_spark = df_spark.drop('visibility')
    print(df_spark.columns)
    # Create a temporary view for your DataFrame
    df_spark.createOrReplaceTempView('Operations')
    # Calculate average temperature per station
    df_avg_temp = df_spark.groupBy('Stations').agg(F.round(F.mean('Temperature'), 2).alias('avg_temp'))
    # Calculate min and max for each attribute per station
    MinMaxPerStation = spark.sql("""
        SELECT Stations,
               ROUND(MAX(Temperature), 2) AS max_temp,
               ROUND(MIN(Temperature), 2) AS min_temp,
               ROUND(MAX(Humidity), 2) AS max_hum,
               ROUND(MIN(Humidity), 2) AS min_hum,
               ROUND(MAX(Rainfall), 2) AS max_RF,
               ROUND(MIN(Rainfall), 2) AS min_RF,
               ROUND(MAX(Wind_Speed), 2) AS max_WS,
               ROUND(MIN(Wind_Speed), 2) AS min_WS,
               ROUND(MAX(Light_Intensity), 2) AS max_LI,
               ROUND(MIN(Light_Intensity), 2) AS min_LI,
               ROUND(MAX(Soil_Moisture), 2) AS max_SO,
               ROUND(MIN(Soil_Moisture), 2) AS min_SO
        FROM Operations
        GROUP BY Stations
    """)
    # Convert the DataFrames to strings
    df_avg_temp_str = df_avg_temp.toPandas().to_string(index=False)
    MinMaxPerStation_str = MinMaxPerStation.toPandas().to_string(index=False)
    # Send the email with the DataFrame strings in the body
    send_email(df_avg_temp_str, MinMaxPerStation_str)
