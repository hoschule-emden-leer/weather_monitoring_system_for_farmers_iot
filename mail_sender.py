from email.message import EmailMessage
import ssl
import smtplib

def send_email(df_avg_temp_str, MinMaxPerStation_str):
    email_sender = '<sender_mail>'
    email_password = '<password>'
    email_receiver = '<receiver_mail>'

    # Set the subject and body of the email
    subject = 'Data_Processed Summary'
    body = f"""
    Average Temperature per Station:
    {df_avg_temp_str}

    Min and Max per Station:
    {MinMaxPerStation_str}
    """

    em = EmailMessage()
    em['From'] = email_sender
    em['To'] = email_receiver
    em['Subject'] = subject
    em.set_content(body)

    # Add SSL (layer of security)
    context = ssl.create_default_context()

    # Log in and send the email
    with smtplib.SMTP_SSL('smtp.gmail.com', 465, context=context) as smtp:
        smtp.login(email_sender, email_password)
        smtp.sendmail(email_sender, email_receiver, em.as_string())
