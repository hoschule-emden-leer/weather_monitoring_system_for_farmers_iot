from flask import Flask, request, jsonify
from pyspark.sql import SparkSession
from pyspark.ml.regression import LinearRegressionModel

app = Flask(__name__)

# Create a SparkSession
spark = SparkSession.builder.appName("loadModel").getOrCreate()

# Load the model
loadedModel = LinearRegressionModel.load("./Regression_models")

from pyspark.ml.feature import VectorAssembler

@app.route('/predict')
def predict_hello():
        return "Predict is working"
@app.route('/predict', methods=['POST'])
def predict():
    try:
        # Get data from the POST request
        data = request.json

        # Convert data to Spark DataFrame
        df = spark.createDataFrame([data], ["feelslike", "dew", "humidity", "precip", "snow", "snowdepth", "windgust", "windspeed", "winddir"])

        # Define the assembler
        assembler = VectorAssembler(
            inputCols=["feelslike", "dew", "humidity", "precip", "snow", "snowdepth", "windgust", "windspeed", "winddir"],
            outputCol='features')

        # Transform the data
        df = assembler.transform(df)

        # Make prediction
        prediction = loadedModel.transform(df)

        # Return prediction
        return jsonify({'prediction': str(prediction.collect())})

    except Exception as e:
        return jsonify({'error': str(e)}), 500

if __name__ == '__main__':
    app.run(host='0.0.0.00',port=5000, debug=True)
