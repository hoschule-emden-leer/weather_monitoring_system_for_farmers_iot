import requests
import json

# Define the url
url = 'http://localhost:5000/predict'

# Define the headers
headers = {'Content-Type': 'application/json'}

# Define the features
features = [4.5, 2.3, 1.2, 50, 0, 0, 10, 5, 180]

# Define the data
# Define the data
data = {
    'feelslike': 5.5,
    'dew': 2.9,
    'humidity': 1.2,
    'precip': 50,
    'snow': 0,
    'snowdepth': 0,
    'windgust': 10,
    'windspeed': 5,
    'winddir': 180
}

# Send the POST request
response = requests.post(url, headers=headers, data=json.dumps(data))

# Print the response
print(response.text)




