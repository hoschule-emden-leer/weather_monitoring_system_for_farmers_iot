import pymysql
import csv
import time
import datetime

# Establish a connection to the MySQL database
connection = pymysql.connect(
    host='weather-data-1.croem02gck4c.eu-central-1.rds.amazonaws.com',
    user='admin',
    password='Admin1234',
    db='weather_database'
)

try:
    with connection.cursor() as cursor:
        # Create a new table
        cursor.execute("""
            CREATE TABLE IF NOT EXISTS weather_sensor_data_new (
                Unnamed INT,
                Date_Time DATETIME,
                Temperature FLOAT,
                Humidity FLOAT,
                Wind_Speed FLOAT,
                Rainfall FLOAT,
                Light_Intensity FLOAT,
                Yield FLOAT,
                Soil_Moisture FLOAT,
                Stations VARCHAR(255),
                visibility FLOAT
            )
        """)

        # Get the latest Date_Time in the database
        cursor.execute("SELECT MAX(Date_Time) FROM weather_sensor_data_new")
        last_date_time = cursor.fetchone()[0]

        while True:  # This will create an infinite loop
            # Open the volume file and read its contents
            with open('output-data.csv', 'r') as f:  # Updated to 'output-data.csv'
                reader = csv.reader(f)
                next(reader)  # Skip the header row
                for row in reader:
                    # Only insert rows that are newer than the last inserted row
                    date_time = datetime.datetime.strptime(row[1], "%Y-%m-%d %H:%M:%S")  # Adjust this based on your date format
                    if date_time > last_date_time:
                        # Prepare the SQL query string
                        sql = "INSERT INTO weather_sensor_data_new (Unnamed, Date_Time, Temperature, Humidity, Wind_Speed, Rainfall, Light_Inte>
                        # Execute the SQL query
                        cursor.execute(sql, row)
                        last_date_time = date_time  # Update the last inserted Date_Time
            # Commit the transaction
            connection.commit()

            # Wait for 5 seconds
            time.sleep(5)
finally:
    # Close the connection
    connection.close()

